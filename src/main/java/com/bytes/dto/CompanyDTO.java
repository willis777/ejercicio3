package com.bytes.dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Andersson
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CompanyDTO {
    
    private Long id;
    private String nombre;
    private String nit;
    private Date fechaFundacion;
    private String direccion;

}
