package com.bytes.mappers;

import com.bytes.dto.CompanyDTO;
import com.bytes.entities.Company;
import org.modelmapper.ModelMapper;

/**
 *
 * @author Andersson
 */
public class CompanyMapper {

    private CompanyMapper() {
        throw new IllegalStateException("CompanyMapper");
    }

    private static final ModelMapper mapper = new ModelMapper();

    public static CompanyDTO mapCompany(Company company) {
        return mapper.map(company, CompanyDTO.class);
    }

    public static Company mapCompanyDTO(CompanyDTO companyDTO) {
        return mapper.map(companyDTO, Company.class);
    }
}
