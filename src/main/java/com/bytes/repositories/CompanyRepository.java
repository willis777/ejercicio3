package com.bytes.repositories;

import com.bytes.entities.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Andersson
 */
@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

}
