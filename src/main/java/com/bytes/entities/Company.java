package com.bytes.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 *
 * @author Andersson
 */
@Entity
@Table(name = "Empresa")
@XmlRootElement
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Company implements Serializable {

    private static final long serialVersionUID = -1214434334566566781L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "Nit")
    private String nit;
    @Basic(optional = false)
    @Column(name = "Fecha_Fundacion")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaFundacion;
    @Basic(optional = false)
    @Column(name = "Direccion")
    private String direccion;
    
   
}
