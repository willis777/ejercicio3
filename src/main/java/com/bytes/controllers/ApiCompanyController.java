package com.bytes.controllers;

import com.bytes.dto.ApiResponseDTO;
import com.bytes.util.Meta;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bytes.services.ApiCompanyService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author Andersson
 */
@RestController
public class ApiCompanyController {

    @Autowired
    private ApiCompanyService apiCompanyService;

    @GetMapping("/hola")
    public ApiResponseDTO hola() {
        return new ApiResponseDTO(new Meta(UUID.randomUUID().toString(), "OK", 200)," Bienvenido a API Empresa V1 ");
    }
    
    @PostMapping("/crear")
    public ApiResponseDTO crearEmpresa(@RequestBody String empresa ) {
        return new ApiResponseDTO(new Meta(UUID.randomUUID().toString(), " Empresa Creada " , 200), apiCompanyService.crearEmpresa(empresa));
    }
    
    @PutMapping("/actualizar")
    public ApiResponseDTO actualizarEmpresa(@RequestBody String empresa ) {
        return new ApiResponseDTO(new Meta(UUID.randomUUID().toString(), " Empresa Actualizada " , 200), apiCompanyService.actualizarEmpresa(empresa));
    }
    
    @GetMapping("/listar")
    public ApiResponseDTO listarEmpresa() {
        return new ApiResponseDTO(new Meta(UUID.randomUUID().toString(), " Empresas " , 200), apiCompanyService.listarEmpresas());
    }
    
    @DeleteMapping("/eliminar")
    public ApiResponseDTO eliminarEmpresa(@RequestBody String empresa ) {
        return new ApiResponseDTO(new Meta(UUID.randomUUID().toString(), " Empresa Eliminada " , 200), apiCompanyService.eliminarEmpresa(empresa));
    }

}
