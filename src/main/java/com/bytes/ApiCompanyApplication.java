package com.bytes;

import com.bytes.config.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class ApiCompanyApplication {
    
    @Autowired
    private AppConfig config;

    public static void main(String[] args) {
        SpringApplication.run(ApiCompanyApplication.class, args);
    }
    
    @Bean
    public WebMvcConfigurer corsCofiConfigurer() {
        return new WebMvcConfigurer() {

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/*").allowedOrigins(config.getAllowedOrigins());
            }
        };
    }

}
