/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bytes.util;

import java.io.Serializable;

/**
 *
 * @author Andersson
 */

public class ValidaAtributo implements Serializable{

    private static final long serialVersionUID = 1096953892289616464L;
    
    private int ordinal;
    private String clase;
    private String tipo;
    private String nombre;
    private String mensaje;
    private String valor;
    private String valorAsignado;

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getValorAsignado() {
        return valorAsignado;
    }

    public void setValorAsignado(String valorAsignado) {
        this.valorAsignado = valorAsignado;
    }

    @Override
    public String toString() {
        return "ValidaAtributo{" + "ordinal=" + ordinal + ", clase=" + clase + ", tipo=" + tipo + ", nombre=" + nombre + ", mensaje=" + mensaje + ", valor=" + valor + ", valorAsignado=" + valorAsignado + '}';
    }

}
