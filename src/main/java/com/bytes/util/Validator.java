/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bytes.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;

/**
 *
 * @author Andersson
 */
public class Validator {

    static List<ValidaAtributo> validaciones = new ArrayList<>();
    static String[] mensajes = {
        "VALOR NO ESPECIFICADO",
        "VALOR NEGATIVO",
        "VALOR VACIO"
    };
    static int i = 0;
    static Logger logger = LoggerFactory.getLogger(Validator.class);
    static final String serialVersionUID = "serialVersionUID";

    private Validator() {

    }

    public static Object validaObjeto(Object pObjeto) {

        Object objeto = pObjeto;

        try {
            Field[] pAtributos = pObjeto.getClass().getDeclaredFields();
            for (Field pField : pAtributos) {
                if (!pField.getName().contains(serialVersionUID)) {
                    ValidaAtributo pValidaAtributo = new ValidaAtributo();
                    boolean isAccesible = pField.isAccessible();
                    pField.setAccessible(true);
                    Object pValor = pField.get(pObjeto);
                    setAtributoByte(pObjeto, pField, pValidaAtributo, pValor);
                    setAtributoShort(pObjeto, pField, pValidaAtributo, pValor);
                    setAtributoInteger(pObjeto, pField, pValidaAtributo, pValor);
                    setAtributoLong(pObjeto, pField, pValidaAtributo, pValor);
                    setAtributoFloat(pObjeto, pField, pValidaAtributo, pValor);
                    setAtributoDouble(pObjeto, pField, pValidaAtributo, pValor);
                    setAtributoCharacter(pObjeto, pField, pValidaAtributo, pValor);
                    setAtributoBigDecimal(pObjeto, pField, pValidaAtributo, pValor);
                    setAtributoString(pObjeto, pField, pValidaAtributo, pValor);
                    setAtributoList(pObjeto, pField, pValidaAtributo, pValor);
                    setAtributoDate(pObjeto, pField, pValidaAtributo, pValor);
                    setAtributoId(pObjeto, pField);
                    pField.setAccessible(isAccesible);
                }
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return objeto;
    }

    public static ValidaAtributo setValida(Object pObjeto, Field pAtributo, int pOrdinal, int pMensaje, ValidaAtributo pValidaAtributo, Object pValor, Object pValorAsignado) {
        try {
            pValidaAtributo.setNombre(pAtributo.getName());
            pValidaAtributo.setTipo(String.valueOf(pAtributo.getType().getName()));
            pValidaAtributo.setClase(pObjeto.getClass().getName());
            pValidaAtributo.setOrdinal(pOrdinal);
            pValidaAtributo.setMensaje(getMensajes()[pMensaje]);
            pValidaAtributo.setValor(String.valueOf(pValor));
            pValidaAtributo.setValorAsignado(String.valueOf(pValorAsignado));
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return pValidaAtributo;
    }
    
    public static void setByte(Field pAtributo, Object pObjeto, Byte pValorAsignado) {
        try {
            pAtributo.set(pObjeto, pValorAsignado);
            i++;
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }
    
    public static int setAsignaByte(Byte pValor, Field pAtributo, Object pObjeto, Byte pValorAsignado) {
        int mensajePosicion = -1;
        try {
            if (pValor == null || pValor == 0) {
                setByte(pAtributo, pObjeto, pValorAsignado);
                mensajePosicion = 0;
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return mensajePosicion;
    }

    public static void setAtributoByte(Object pObjeto, Field pAtributo, ValidaAtributo pValidaAtributo, Object pValor) {
        try {
            if (pAtributo.getType().equals(java.lang.Byte.class) || pAtributo.getType().equals(Byte.TYPE)) {
                Byte valor = (Byte) pValor;
                Byte valorAsignado = new Byte("0");
                int mensajePosicion = setAsignaByte(valor, pAtributo, pObjeto, valorAsignado);
                if(mensajePosicion>=0){
                    getValidaciones().add(setValida(pObjeto, pAtributo, i, 0, pValidaAtributo, pValor, (Object) valorAsignado));
                }
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static void setShort(Field pAtributo, Object pObjeto, Short pValorAsignado) {
        try {
            pAtributo.set(pObjeto, pValorAsignado);
            i++;
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static int setAsignaShort(Short pValor, Field pAtributo, Object pObjeto, Short pValorAsignado) {
        int mensajePosicion = -1;
        try {
            if (pValor == null || pValor == 0) {
                setShort(pAtributo, pObjeto, pValorAsignado);
                mensajePosicion = 0;
            } else if (pValor < 0) {
                mensajePosicion = 1;
                setShort(pAtributo, pObjeto, pValorAsignado);
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return mensajePosicion;
    }

    public static void setAtributoShort(Object pObjeto, Field pAtributo, ValidaAtributo pValidaAtributo, Object pValor) {
        try {
            if (pAtributo.getType().equals(java.lang.Short.class) || pAtributo.getType().equals(Short.TYPE)) {
                Short valor = (Short) pValor;
                Short valorAsignado = new Short("0");
                int mensajePosicion = setAsignaShort(valor, pAtributo, pObjeto, valorAsignado);
                if (mensajePosicion >= 0) {
                    getValidaciones().add(setValida(pObjeto, pAtributo, i, mensajePosicion, pValidaAtributo, pValor, (Object) valorAsignado));
                }
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static void setInteger(Field pAtributo, Object pObjeto, Object pValorAsignado) {
        try {
            pAtributo.set(pObjeto, pValorAsignado);
            i++;
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static int setAsignaInteger(Integer pValor, Field pAtributo, Object pObjeto, Object pValorAsignado) {
        int mensajePosicion = -1;
        try {
            if (pValor == null || pValor == 0) {
                setInteger(pAtributo, pObjeto, pValorAsignado);
                mensajePosicion = 0;
            } else if (pValor < 0) {
                mensajePosicion = 1;
                setInteger(pAtributo, pObjeto, pValorAsignado);
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return mensajePosicion;
    }

    public static void setAtributoInteger(Object pObjeto, Field pAtributo, ValidaAtributo pValidaAtributo, Object pValor) {
        try {
            if (pAtributo.getType().equals(java.lang.Integer.class) || pAtributo.getType().equals(Integer.TYPE)) {
                Integer valor = (Integer) pValor;
                Object valorAsignado = 0;
                int mensajePosicion = setAsignaInteger(valor, pAtributo, pObjeto, valorAsignado);
                if (mensajePosicion >= 0) {
                    getValidaciones().add(setValida(pObjeto, pAtributo, i, mensajePosicion, pValidaAtributo, pValor, valorAsignado));
                }
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static void setLong(Field pAtributo, Object pObjeto, Object pValor) {
        try {
            pAtributo.set(pObjeto, pValor);
            i++;
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static int setAsignaLong(Long pValor, Field pAtributo, Object pObjeto, Object pValorAsignado) {
        int mensajePosicion = -1;
        try {
            if (pValor == null || pValor == 0) {
                setLong(pAtributo, pObjeto, pValorAsignado);
                mensajePosicion = 0;
            } else if (pValor < 0) {
                mensajePosicion = 1;
                setLong(pAtributo, pObjeto, pValorAsignado);
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return mensajePosicion;
    }

    public static void setAtributoLong(Object pObjeto, Field pAtributo, ValidaAtributo pValidaAtributo, Object pValor) {
        try {
            if (pAtributo.getType().equals(java.lang.Long.class) || pAtributo.getType().equals(Long.TYPE)) {
                Long valor = (Long) pValor;
                Object valorAsignado = 0L;
                int mensajePosicion = setAsignaLong(valor, pAtributo, pObjeto, valorAsignado);
                if (mensajePosicion >= 0) {
                    getValidaciones().add(setValida(pObjeto, pAtributo, i, mensajePosicion, pValidaAtributo, pValor, valorAsignado));
                }
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static void setFloat(Field pAtributo, Object pObjeto, Object pValor) {
        try {
            pAtributo.set(pObjeto, pValor);
            i++;
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static int setAsignaFloat(Float pValor, Field pAtributo, Object pObjeto, Object pValorAsignado) {
        int mensajePosicion = -1;
        try {
            if (pValor == null || pValor == 0) {
                setFloat(pAtributo, pObjeto, pValorAsignado);
                mensajePosicion = 0;
            } else if (pValor < 0) {
                mensajePosicion = 1;
                setFloat(pAtributo, pObjeto, pValorAsignado);
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return mensajePosicion;
    }

    public static void setAtributoFloat(Object pObjeto, Field pAtributo, ValidaAtributo pValidaAtributo, Object pValor) {
        try {
            if (pAtributo.getType().equals(java.lang.Float.class) || pAtributo.getType().equals(Float.TYPE)) {
                Float valor = (Float) pValor;
                Object valorAsignado = 0F;
                int mensajePosicion = setAsignaFloat(valor, pAtributo, pObjeto, valorAsignado);
                if (mensajePosicion >= 0) {
                    getValidaciones().add(setValida(pObjeto, pAtributo, i, mensajePosicion, pValidaAtributo, pValor, valorAsignado));
                }
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static void setDouble(Field pAtributo, Object pObjeto, Object pValor) {
        try {
            pAtributo.set(pObjeto, pValor);
            i++;
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static int setAsignaDouble(Double pValor, Field pAtributo, Object pObjeto, Object pValorAsignado) {
        int mensajePosicion = -1;
        try {
            if (pValor == null || pValor == 0) {
                setDouble(pAtributo, pObjeto, pValorAsignado);
                mensajePosicion = 0;
            } else if (pValor < 0) {
                mensajePosicion = 1;
                setDouble(pAtributo, pObjeto, pValorAsignado);
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return mensajePosicion;
    }

    public static void setAtributoDouble(Object pObjeto, Field pAtributo, ValidaAtributo pValidaAtributo, Object pValor) {
        try {
            if (pAtributo.getType().equals(java.lang.Double.class) || pAtributo.getType().equals(Double.TYPE)) {
                Double valor = (Double) pValor;
                Object valorAsignado = 0D;
                int mensajePosicion = setAsignaDouble(valor, pAtributo, pObjeto, valorAsignado);
                if (mensajePosicion >= 0) {
                    getValidaciones().add(setValida(pObjeto, pAtributo, i, mensajePosicion, pValidaAtributo, pValor, valorAsignado));
                }
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static void setAtributoCharacter(Object pObjeto, Field pAtributo, ValidaAtributo pValidaAtributo, Object pValor) {
        try {
            if (pAtributo.getType().equals(java.lang.Character.class) || pAtributo.getType().equals(Character.TYPE)) {
                Character valor = (Character) pValor;
                if (valor == null) {
                    char temp = '0';
                    pAtributo.set(pObjeto, temp);
                    i++;
                    getValidaciones().add(setValida(pObjeto, pAtributo, i, 0, pValidaAtributo, pValor, (Object) temp));
                }
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static void setBigDecimal(Field pAtributo, Object pObjeto, Object pValor) {
        try {
            pAtributo.set(pObjeto, pValor);
            i++;
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static int setAsignaBigDecimal(BigDecimal pValor, Field pAtributo, Object pObjeto, Object pValorAsignado) {
        int mensajePosicion = -1;
        try {
            if (pValor == null || pValor.compareTo(BigDecimal.ZERO) == 0) {
                setBigDecimal(pAtributo, pObjeto, pValorAsignado);
                mensajePosicion = 0;
            } else if (pValor.compareTo(BigDecimal.ZERO) < 0) {
                mensajePosicion = 1;
                setBigDecimal(pAtributo, pObjeto, pValorAsignado);
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return mensajePosicion;
    }

    public static void setAtributoBigDecimal(Object pObjeto, Field pAtributo, ValidaAtributo pValidaAtributo, Object pValor) {
        try {
            if (pAtributo.getType().equals(java.math.BigDecimal.class)) {
                BigDecimal valor = (BigDecimal) pValor;
                BigDecimal valorAsignado = BigDecimal.ZERO;
                int mensajePosicion = setAsignaBigDecimal(valor, pAtributo, pObjeto, valorAsignado);
                if (mensajePosicion >= 0) {
                    getValidaciones().add(setValida(pObjeto, pAtributo, i, mensajePosicion, pValidaAtributo, pValor, (Object) valorAsignado));
                }
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static void setString(Field pAtributo, Object pObjeto, String pValorAsignado) {
        try {
            pAtributo.set(pObjeto, pValorAsignado);
            i++;
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }
    
    public static int setAsignaString(String pValor, Field pAtributo, Object pObjeto, String pValorAsignado) {
        int mensajePosicion = -1;
        try {
            if (pValor == null) {
                setString(pAtributo, pObjeto, pValorAsignado);
                mensajePosicion = 0;
            } else if (pValor.isEmpty()) {
                mensajePosicion = 2;
                setString(pAtributo, pObjeto, pValorAsignado);
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return mensajePosicion;
    }
    
    public static void setAtributoString(Object pObjeto, Field pAtributo, ValidaAtributo pValidaAtributo, Object pValor) {
        try {
            if (pAtributo.getType().equals(java.lang.String.class)) {
                String valor = (String) pValor;
                String valorAsignado = "-";
                int mensajePosicion = setAsignaString(valor, pAtributo, pObjeto, valorAsignado);
                if (mensajePosicion >= 0) {
                    getValidaciones().add(setValida(pObjeto, pAtributo, i, mensajePosicion, pValidaAtributo, pValor, valorAsignado));
                } 
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }


    public static void setAtributoList(Object pObjeto, Field pAtributo, ValidaAtributo pValidaAtributo, Object pValor) {
        try {
            if (pAtributo.getType().equals(java.util.List.class)) {
                List objetosList = (ArrayList) pValor;
                List valorAsignado = new ArrayList<>();
                if (objetosList == null) {
                    pAtributo.set(pObjeto, valorAsignado);
                    i++;
                    getValidaciones().add(setValida(pObjeto, pAtributo, i, 0, pValidaAtributo, pValor, mensajes[2]));
                } else {
                    for (Object objetoFromList : objetosList) {
                        validaObjeto(objetoFromList);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static void setAtributoDate(Object pObjeto, Field pAtributo, ValidaAtributo pValidaAtributo, Object pValor) {
        try {
            if (pAtributo.getType().equals(java.util.Date.class)) {
                Date valor = (Date) pValor;
                if (valor == null) {
                    pAtributo.set(pObjeto, new Date());
                    i++;
                    getValidaciones().add(setValida(pObjeto, pAtributo, i, 0, pValidaAtributo, pValor, (Object) new Date()));
                }
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static void setAtributoId(Object pObjeto, Field pAtributo) {
        try {
            Annotation annotationId = pAtributo.getAnnotation(Id.class);
            if (annotationId != null) {
                pAtributo.set(pObjeto, null);
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static List<ValidaAtributo> getValidaciones() {
        return validaciones;
    }

    public static void setValidaciones(List<ValidaAtributo> validaciones) {
        Validator.validaciones = validaciones;
    }

    public static String[] getMensajes() {
        return mensajes;
    }

    public static void setMensajes(String[] mensajes) {
        Validator.mensajes = mensajes;
    }

    public static int getI() {
        return i;
    }

}
