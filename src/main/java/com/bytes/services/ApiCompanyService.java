package com.bytes.services;

import org.springframework.stereotype.Service;

/**
 *
 * @author Andersson
 */
@Service
public interface ApiCompanyService {
    public Object crearEmpresa(String empresa);
    public Object actualizarEmpresa(String empresa);
    public Object eliminarEmpresa(String empresa);
    public Object listarEmpresas();
}
