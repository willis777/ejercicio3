package com.bytes.services.impl;

import com.bytes.dto.CompanyDTO;
import com.bytes.entities.Company;
import com.bytes.mappers.CompanyMapper;
import com.bytes.repositories.CompanyRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import com.bytes.services.ApiCompanyService;
import com.bytes.util.Validator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Andersson
 */
@Service
public class ApiCompanyServiceImpl implements ApiCompanyService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String MENSAJEBADREQUEST = "La información que envió no tiene el formato correcto.";
    
    @Autowired
    private CompanyRepository companyRepository;


    @Override
    public Object crearEmpresa(String empresa) {
        ArrayList<Object> objects = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
        try {
            
            CompanyDTO CompanyDTO = objectMapper.readValue(empresa, CompanyDTO.class);
            Company company = CompanyMapper.mapCompanyDTO(CompanyDTO);
            Validator.validaObjeto(company);
            this.companyRepository.save(company);
            objects.add(company);

        } catch (Exception e) {
            logger.error("Error ApiSearchServiceImpl crearEmpresa ", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, MENSAJEBADREQUEST);
        }

        return objects;
    }

    @Override
    public Object actualizarEmpresa(String empresa) {
        ArrayList<Object> objects = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
        try {
            
            CompanyDTO CompanyDTO = objectMapper.readValue(empresa, CompanyDTO.class);
            Company company = CompanyMapper.mapCompanyDTO(CompanyDTO);
            Validator.validaObjeto(company);
            this.companyRepository.save(company);
            objects.add(company);

        } catch (Exception e) {
            logger.error("Error ApiSearchServiceImpl actualizarEmpresa ", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, MENSAJEBADREQUEST);
        }

        return objects;
    }

    @Override
    public Object eliminarEmpresa(String empresa) {
        ArrayList<Object> objects = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
        try {
            
            
            CompanyDTO CompanyDTO = objectMapper.readValue(empresa, CompanyDTO.class);
            Company company = CompanyMapper.mapCompanyDTO(CompanyDTO);
            Validator.validaObjeto(company);
            this.companyRepository.delete(company);
            objects.add(company);
            

        } catch (Exception e) {
            logger.error("Error ApiSearchServiceImpl eliminarEmpresa ", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, MENSAJEBADREQUEST);
        }

        return objects;
    }

    @Override
    public Object listarEmpresas() {
        ArrayList<Object> objects = new ArrayList<>();

        try {
            

            this.companyRepository.findAll().forEach((company) -> {
                objects.add(CompanyMapper.mapCompany(company));
            });

        } catch (Exception e) {
            logger.error("Error ApiSearchServiceImpl search", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, MENSAJEBADREQUEST);
        }

        return objects;
    }

}
