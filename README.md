
=======
# README #
# Bienvenido a API Empresa

* Únicamente es necesario ejecutar lo siguiente en una terminal 

### Ejercicio 1  ###

java -jar Ejercicio1.jar

### Ejercicio 2  ###

java -jar Ejercicio2.jar

### Ejercicio 3  ###

1) Ejecute el script byte.sql en MYSQL
2) Ajuste application.properties de acuerdo a sus credenciales
3) Ejecute el siguiente comando java -jar APIEmpresa-1.0.0.jar --spring.config.location=application.properties 
4) Restaurar la coleccion de PostMan en el cual se incluyen todos los URIS, asi mismo puede probarse la funcionalidad de la API , siendo estos los siguientes
4.1) http://localhost:8080/api/v1/hola
4.2) http://localhost:8080/api/v1/crear
4.3) http://localhost:8080/api/v1/actualizar
4.4) http://localhost:8080/api/v1/eliminar
4.5) http://localhost:8080/api/v1/listar

