CREATE DATABASE BYTE;

CREATE TABLE Empresa ( Id int NOT NULL AUTO_INCREMENT, Nombre varchar(255) NOT NULL, Nit varchar(45) NOT NULL, Fecha_Fundacion DATE, Direccion varchar(255), PRIMARY KEY (Id));